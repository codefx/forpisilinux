<p>This is for <strong>Pisilinux 2.0 or higher</strong>.</p>
<p> DotnetCore SDK's <strong>pisi</strong> packages  
</p>
<p> I made that  for myself but you can use it, too.</p>

<p>for use it:<br/>

1 - You must have  <a href="https://www.pisilinux.org"><i> Pisilinux 2.0 or higher </i></a> :)<br/>

and  open a <em>console</em>,<br/> 

2-  type that,<br/> <code> pisi it -c system.devel </code><br/>, these packages are 
necessary for package building.<br/>

then answer the question as "yes" or "y", for Turkish "Evet" or "e"<br/>

this will take a  time that is up to your download speed. please be patient.<br/>
3- when finished,<br/>

for <strong> DotnetCore **2** Sdk</strong> you type that:<br/>
<code> sudo pisi bi https://gitlab.com/codefx/forpisilinux/raw/master/DotnetCoreSDK/pspec.xml </code><br/>

for <strong> DotnetCore **3** Sdk</strong> you type that:<br/>
<code> sudo pisi bi https://gitlab.com/codefx/forpisilinux/raw/master/DotnetCore-3-SDK/pspec.xml </code><br/>

for <strong>Visual studio Code </strong>you look at <a href= "https://github.com/pisilinux/pisilife-2/tree/master/VScode">pisilife-2 repos</a><br/>


this will create the package/s on your default location on your pc.<br/>
when the package/s was/were created, type <br/><code> pisi it *pisi</code><br/>
that's it.<br/>
 enjoy it!