#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/copyleft/gpl.txt
from pisi.actionsapi import autotools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools


WorkDir = "."
NoStrip = ["/"]

def install():
    pisitools.insinto("/usr/share/dotnet", "*")     
    pisitools.dosym("/usr/share/dotnet/dotnet", "/usr/bin/dotnet")

# Package Name : dotnet
# Version : 3.1.101
# Summary : Microsoft DotnetCore SDK

# For more information, you can look at the Actions API
# from the Help menu and toolbar.
